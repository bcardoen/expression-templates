#include <iostream>
#include "expression.h"

int main(void)
{
        std::vector<double> a(100000, 42);
        std::vector<double> b(100000, 3.14);
        auto left = makeleaf(a);
        auto right = makeleaf(b);
        std::cout<< eval( (left+right*left+right)/left, 999)<<std::endl;
}