/**
 *  Self study expression template, based on B. Nikolic's examples (http://www.bnikolic.co.uk/blog/cpp-expression-minimal.html), wikipedia and the book C++ Templates, the Complete Guide.
 *  @author Ben Cardoen, February 2016
 *  The Problem : for large N, vectors (or matrices) in expressions such as (a + b * c/e)[2] generates temporaries, for the entire expression (and not just [2].
 *  A Solution : Use lazy evaluation by way of expression templates to construct a compile time AST of the expression. 
 *  Pro : lazy evaluation, copies elided.
 *  Con : harder to parallelise, and possible loss of context, needs compiler optimisation.
 */
#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <vector>
#include <cstdlib>
#include <iostream>

typedef double fp_t;
typedef std::vector<fp_t> evector;

// Ideally, we want this code to work not just with vectors of double, but anything that has the correct operator definitions.
template<typename T>
using operand=std::vector<T>;

// Operator Traits
// Braces can be omitted, since there is no instantiation ever of one of these types, so they can remain incomplete.
struct Addition {};
struct Subtraction {};
struct Multiplication {};
struct Division{};

// This is the core Type of the entire AST.
template<typename T=fp_t, typename LeftExpr=operand<T>,
         typename OperatorType=void,typename RightExpr=operand<T>>
struct BinaryExpr
{
  const LeftExpr &l;
  const RightExpr &r;
  
  // Useful to retrieve Node type in temporary expressions
  typedef OperatorType optype_t;

  // Tree node
  BinaryExpr(const LeftExpr &left,
        const RightExpr &right):
    l(left),
    r(right)
  {
  };

  // Leaf node
  BinaryExpr(const LeftExpr &val):l(val),r(val){;}
};

// Evaluation functions
// Leaf node, return value.
template<typename T=fp_t, typename LExpr>
T eval(const BinaryExpr<T, LExpr, void, LExpr> &o,
            size_t i)
{
  return o.l[i];
}

// Addition of an expression delegates recursively to the leaves.
template<typename T=fp_t, typename LeftExpr, typename RightExpr>
T eval(const BinaryExpr<T, LeftExpr, Addition, RightExpr> &o, size_t i)
{
  return eval(o.l,i)+eval(o.r,i);
}

template<typename T=fp_t, typename LeftExpr, typename RightExpr>
T eval(const BinaryExpr<T, LeftExpr, Subtraction, RightExpr> &o, size_t i)
{
  return eval(o.l,i) - eval(o.r,i);
}

template<typename T=fp_t, typename LeftExpr, typename RightExpr>
T eval(const BinaryExpr<T, LeftExpr, Multiplication, RightExpr> &o, size_t i)
{
  return eval(o.l,i)*eval(o.r,i);
}

template<typename T=fp_t, typename LeftExpr, typename RightExpr>
T eval(const BinaryExpr<T, LeftExpr, Division, RightExpr> &o, size_t i)
{
  return eval(o.l,i) / eval(o.r,i);
}

// Apart from the evaluation function, we also need creator functions to make expressions from expressions,
// this ultimately creates the ast. (thanks g++)
// Also, it wouldn't be C++ if we don't play with some operator overloading, now wouldn't it ?

template<typename T=fp_t, typename LeftExpr, typename RightExpr>
auto operator+ (const LeftExpr &left, const RightExpr &right)
{
  return BinaryExpr<T, LeftExpr, Addition, RightExpr>(left, right);
}

template<typename T=fp_t, typename LeftExpr, typename RightExpr>
auto operator- (const LeftExpr &left, const RightExpr &right)
{
  return BinaryExpr<T, LeftExpr, Subtraction, RightExpr>(left, right);
}

template<typename T=fp_t, typename LeftExpr, typename RightExpr>
auto operator* (const LeftExpr &left, const RightExpr &right)
{
  return BinaryExpr<T, LeftExpr, Multiplication, RightExpr>(left, right);
}

template<typename T=fp_t, typename LeftExpr, typename RightExpr>
auto operator/ (const LeftExpr &left, const RightExpr &right)
{
  return BinaryExpr<T, LeftExpr, Division, RightExpr>(left, right);
}

// We don't need this one exactly, but it does avoid quite a bit of instantiation.
template <typename T=fp_t, typename LExpr>
auto makeleaf(const LExpr &val)
{
  return BinaryExpr<T, LExpr, void, LExpr>(val);
}

#endif //guard