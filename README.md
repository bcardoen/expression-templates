# README #

### Experiments with expression templates ###

A smallish implementation of vector expression templates, to get a feeling for how they work. 

### Requirements ###
* C++14 conformant compiler (e.g. g++ 5.3)
* CMake

### Building ###
    $mkdir build/{debug|release|benchmark} && cd build/{debug|release|benchmark}
    $cmake -DCMAKE_BUILD_TYPE=Benchmark ../../main  
    $make 
    $./expressiontemplate

